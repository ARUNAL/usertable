import { Form, Formik } from 'formik'
import React, { useState, useEffect } from 'react'

const EditUserForm = props => {
  const [user, setUser] = useState(props.currentUser)

  useEffect(
    () => {
      setUser(props.currentUser)
    },
    [props]
  )
  // You can tell React to skip applying an effect if certain values haven’t changed between re-renders. [ props ]

  const handleInputChange = event => {
    const { name, value } = event.target

    setUser({ ...user, [name]: value })
  }

  return (
    <Formik>

    
    <Form
    dataSource ={user}
    >
      <label>Name</label>
      <input type="text" name="name" value={user.name} onChange={handleInputChange} />
      <label>mail</label>
      <input type="text" name="mail" value={user.mail} onChange={handleInputChange} />
      <label>Phone</label>
      <input type="number" name="phone" value={user.phone} onChange={handleInputChange} />
      <button onClick={() => props.updateUser(user.id, user)}>Update</button>
      <button onClick={() => props.setEditing(false)} className="button muted-button">
        Cancel
      </button>
    </Form>
    </Formik>
  )
}

export default EditUserForm
