import React, { useState } from 'react'
import { Formik,Field } from 'formik'
import { Form,  } from 'antd'
import * as Yup from "yup";

const SignupSchema = Yup.object().shape({
	name: Yup.string()
	  .min(3, 'Too Short!')
	  .max(50, 'Too Long!')
	  .required('Required'),
	phone: Yup.number()
	  .min(8, 'Too Short!')
	  .max(15, 'Too Long!')
	  .required('Required'),
	mail: Yup.string().email('Invalid email').required('Required'),
  });
  
const AddUserForm = props => {
	const initialFormState = { id: null, name: '', mail: '', phone: '' }
	const [user, setUser] = useState(initialFormState)

	const handleFieldChange = event => {
		const { name, value } = event.target

		setUser({ ...user, [name]: value })
	}
	

	return (
		<Formik
		initialValues={{
			name: '',
			number: '',
			email: '',
		  }}
		  validationSchema={SignupSchema}

		> 
			{({ errors, touched }) => (
			<Form 
			
			dataSource ={user}
			>
			
			<div>
				<p><label>Name:</label></p>
				<Field type="text" name="name" value={user.name}  onChange={handleFieldChange} />
				{errors.name && touched.name ? (
             <div>{errors.name}</div>
           ) : null}
				<p><label>mail:</label></p>
				<Field type="text" name="mail" value={user.mail}   onChange={handleFieldChange} />
				{errors.mail && touched.mail ? <div>{errors.mail}</div> : null}
           <p><label>Phone:</label></p>
				<Field type="number" name="phone" value={user.phone} onChange={handleFieldChange} />
				{errors.number && touched.number ? (
             <div>{errors.number}</div>
           ) : null}
				<p><button onClick={() => props.addUser(user)}>Add </button></p>
			</div>
			</Form>
		)}
		</Formik>



		
	)
}

export default AddUserForm
